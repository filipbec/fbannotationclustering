//
//  FBViewController.h
//  AnnotationClustering
//
//  Created by Filip Bec on 05/01/14.
//  Copyright (c) 2014 Infinum Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FBQuadTreeCoordinator.h"

@interface FBViewController : UIViewController <MKMapViewDelegate, FBQuadTreeCoordinatorDelegate>

@end
