//
//  FBViewController.m
//  AnnotationClustering
//
//  Created by Filip Bec on 05/01/14.
//  Copyright (c) 2014 Infinum Ltd. All rights reserved.
//

#import "FBViewController.h"
#import "FBAnnotation.h"
#import "FBAnnotationCluster.h"
#import "FBQuadTreeCoordinator.h"

@interface FBViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, retain) FBQuadTreeCoordinator *treeCoordinator;

@end

@implementation FBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.mapView.delegate = self;
    
    // Generating annotations
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 100000; i++) {
        FBAnnotation *a = [[FBAnnotation alloc] init];
        a.coordinate = CLLocationCoordinate2DMake(drand48() * 160 - 80, drand48() * 360 - 180);
        
        [array addObject:a];
    }
    
    // Building tree
    self.treeCoordinator = [[FBQuadTreeCoordinator alloc] init];
    self.treeCoordinator.delegate = self;
    [self.treeCoordinator buildTreeWithAnnotations:array];
    
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(0, 0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    // prefferable way to update clustered annotations
    [[NSOperationQueue new] addOperationWithBlock:^{
        double scale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
        NSArray *annotations = [self.treeCoordinator clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
        
        [self.treeCoordinator setAnnotations:annotations forMapView:self.mapView];
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *const AnnotatioViewReuseID = @"AnnotatioViewReuseID";
    
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotatioViewReuseID];
    
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotatioViewReuseID];
    }

    if ([annotation isKindOfClass:[FBAnnotationCluster class]]) {
        // Annotation is cluster
        FBAnnotationCluster *cl = (FBAnnotationCluster*)annotation;
        cl.title = [NSString stringWithFormat:@"%lu", (unsigned long)cl.annotations.count];
        
        annotationView.pinColor = MKPinAnnotationColorGreen;
        annotationView.canShowCallout = YES;
    } else {
        // Annotation is single location - my custom annotation
        annotationView.pinColor = MKPinAnnotationColorRed;
        annotationView.canShowCallout = NO;
    }

    return annotationView;
}

#pragma mark - FBQuadTreeCoordinator delegate
- (CGFloat)cellSizeFactorForCoordinator:(FBQuadTreeCoordinator *)coordinator
{
    return 1.5;
}

@end
