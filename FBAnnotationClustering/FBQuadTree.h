//
//  FBQuadTree.h
//  AnnotationClustering
//
//  Created by Filip Bec on 05/01/14.
//  Copyright (c) 2014 Infinum Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBQuadTreeNode.h"

/**
 Quad Tree. You should never use this class.
 */
@interface FBQuadTree : NSObject

@property (nonatomic, retain) FBQuadTreeNode *rootNode;

- (BOOL)insertAnnotation:(id<MKAnnotation>)annotation;
- (void)enumerateAnnotationsInBox:(FBBoundingBox)box usingBlock:(void (^)(id<MKAnnotation> obj))block;
- (void)enumerateAnnotationsUsingBlock:(void (^)(id<MKAnnotation> obj))block;

@end
