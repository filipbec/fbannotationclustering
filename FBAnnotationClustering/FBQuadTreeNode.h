//
//  FBQuadTreeNode.h
//  AnnotationClustering
//
//  Created by Filip Bec on 05/01/14.
//  Copyright (c) 2014 Infinum Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

typedef struct {
    CGFloat x0;
    CGFloat y0;
    CGFloat xf;
    CGFloat yf;
} FBBoundingBox;

FBBoundingBox FBBoundingBoxMake(CGFloat x0, CGFloat y0, CGFloat xf, CGFloat yf);

FBBoundingBox FBBoundingBoxForMapRect(MKMapRect mapRect);
MKMapRect FBMapRectForBoundingBox(FBBoundingBox boundingBox);

BOOL FBBoundingBoxContainsCoordinate(FBBoundingBox box, CLLocationCoordinate2D coordinate);
BOOL FBBoundingBoxIntersectsBoundingBox(FBBoundingBox box1, FBBoundingBox box2);


/**
 Quad Tree Node. You should never use this class.
 */
@interface FBQuadTreeNode : NSObject

@property (nonatomic, assign) NSUInteger count;
@property (nonatomic, assign) FBBoundingBox boundingBox;

@property (nonatomic, retain) NSMutableArray *annotations;

@property (nonatomic, retain) FBQuadTreeNode *northEast;
@property (nonatomic, retain) FBQuadTreeNode *northWest;
@property (nonatomic, retain) FBQuadTreeNode *southEast;
@property (nonatomic, retain) FBQuadTreeNode *southWest;

- (id)initWithBoundingBox:(FBBoundingBox)box;
- (BOOL)isLeaf;
- (void)subdivide;

@end
